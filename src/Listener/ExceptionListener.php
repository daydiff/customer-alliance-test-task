<?php

declare(strict_types=1);

namespace App\Listener;

use App\Exception\DomainException;
use App\Exception\InvalidRequestException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    private LoggerInterface $logger;
    private bool $debug;

    public function __construct(LoggerInterface $logger, bool $debug)
    {
        $this->logger = $logger;
        $this->debug = $debug;
    }

    /**
     * Handles the onKernelException event.
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $e = $event->getThrowable();

        if ($e instanceof InvalidRequestException) {
            $event->setResponse(
                new JsonResponse(
                    [
                        'errors' => $e->getErrors()
                    ],
                    Response::HTTP_BAD_REQUEST
                )
            );

            return;
        }

        if ($e instanceof DomainException) {
            $event->setResponse(
                new JsonResponse(
                    [
                        'errors' => ['message' => $e->getMessage()]
                    ],
                    Response::HTTP_BAD_REQUEST
                )
            );

            return;
        }

        $this->defaultHandler($e, $event);
    }

    private function defaultHandler(\Throwable $e, ExceptionEvent $event)
    {
        if (!$this->debug) {
            $event->setResponse(
                new JsonResponse(
                    null,
                    Response::HTTP_INTERNAL_SERVER_ERROR
                )
            );

            return;
        }

        $error = [
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => $e->getTraceAsString(),
        ];

        if ($e->getPrevious()) {
            $error['previous'] = [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ];
        }

        $event->setResponse(
            new JsonResponse(
                [
                    'errors' => $error
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            )
        );
    }
}
