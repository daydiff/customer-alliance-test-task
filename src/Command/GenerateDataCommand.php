<?php

declare(strict_types=1);

namespace App\Command;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GenerateDataCommand extends Command
{
    private const BATCH_SIZE = 50;

    /**
     * @var string
     */
    protected static $defaultName = 'generate:data';

    private Connection $connection;

    public function __construct(Connection $connection, ?string $name = null)
    {
        parent::__construct($name);
        $this->connection = $connection;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this->setDescription('Generate data');
    }

    /**
     * {@inheritDoc}
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->connection->executeQuery("TRUNCATE TABLE `hotels`");
        $this->connection->executeQuery("TRUNCATE TABLE `reviews`");

        for ($i = 1; $i < 11; $i++) {
            $name = 'Hotel ' . rand(1000, 1000000);
            $this->connection->executeQuery("INSERT INTO `hotels` (`id`, `name`) VALUES (?, ?)", [$i, $name]);
        }

        $comments = [
            'Awesome place, recommend',
            'Super cool',
            'All was good',
            'Worse hotel ever',
            'Best hotel ever',
            'Was ok',
            'Not that bad',
            'Good location',
            'Breakfasts suck',
            'Whatever',
        ];
        for ($i = 1; $i < 100001; $i++) {
            $hotelId = rand(1, 10);
            $comment = $comments[rand(0, count($comments) - 1)];
            $score = rand(1, 10);
            $createdDate = $this->getCreatedDate();
            $this->connection->executeQuery(
                "INSERT INTO `reviews` (`id`, `hotel_id`, `score`, `comment`, `created_date`) VALUES (?, ?, ?, ?, ?)",
                [$i, $hotelId, $score, $comment, $createdDate->format('c')]
            );
        }

        $io->success('Finished');

        return 0;
    }

    protected function getCreatedDate(): \DateTimeInterface
    {
        $toDate = time();
        $fromDate = $toDate - (60 * 60 * 24 * 365 * 2);
        $date = rand($fromDate, $toDate);

        return \DateTime::createFromFormat('U', (string)$date);
    }
}
