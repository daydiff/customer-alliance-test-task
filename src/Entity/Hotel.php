<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hotels")
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 */
class Hotel
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private \DateTimeInterface $createdDate;

    /**
     * @ORM\JoinColumn(name="id", referencedColumnName="hotel_id", nullable=true)
     * @ORM\OneToMany(targetEntity="Review", mappedBy="hotel", cascade={"persist"})
     */
    private Collection $reviews;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Hotel
     */
    public function setId(int $id): Hotel
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Hotel
     */
    public function setName(string $name): Hotel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     * @return Hotel
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): Hotel
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    /**
     * @param Collection $reviews
     * @return Hotel
     */
    public function setReviews(Collection $reviews): Hotel
    {
        $this->reviews = $reviews;
        return $this;
    }
}
