<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="reviews")
 * @ORM\Entity(repositoryClass="App\Repository\ReviewRepository")
 */

class Review
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="reviews")
     * @ORM\JoinColumn(name="hotel_id", referencedColumnName="id")
     */
    private Hotel $hotel;

    /**
     * @ORM\Column(name="score", type="integer", nullable=false)
     */
    private string $score;

    /**
     * @ORM\Column(name="comment", type="string", nullable=false)
     */
    private string $comment;

    /**
     * @ORM\Column(name="created_date", type="datetime", nullable=false)
     */
    private \DateTimeInterface $createdDate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Review
     */
    public function setId(int $id): Review
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * @param Hotel $hotel
     * @return Review
     */
    public function setHotel(Hotel $hotel): Review
    {
        $this->hotel = $hotel;
        return $this;
    }

    /**
     * @return string
     */
    public function getScore(): string
    {
        return $this->score;
    }

    /**
     * @param string $score
     * @return Review
     */
    public function setScore(string $score): Review
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return Review
     */
    public function setComment(string $comment): Review
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedDate(): \DateTimeInterface
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTimeInterface $createdDate
     * @return Review
     */
    public function setCreatedDate(\DateTimeInterface $createdDate): Review
    {
        $this->createdDate = $createdDate;
        return $this;
    }
}
