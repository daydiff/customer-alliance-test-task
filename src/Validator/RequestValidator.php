<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\InvalidRequestException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestValidator implements RequestValidatorInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function validate(object $request): void
    {
        $validationErrors = $this->validator->validate($request);

        if (count($validationErrors) === 0) {
            return;
        }

        $errors = [];

        foreach ($validationErrors as $validationError) {
            $errors[$validationError->getPropertyPath()] = $validationError->getMessage();
        }

        throw new InvalidRequestException($errors);
    }
}