<?php

declare(strict_types=1);

namespace App\Validator;

use App\Exception\InvalidRequestException;

interface RequestValidatorInterface
{
    /**
     * @throws InvalidRequestException
     */
    public function validate(object $obj);
}
