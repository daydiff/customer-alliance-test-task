<?php

declare(strict_types=1);

namespace App\Controller;

use App\Builder\OvertimeDTOBuilder;
use App\DataReader\OvertimeDataReader;
use App\Validator\RequestValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\DTO\Response\OvertimeReviewDTO;

/**
 * @OA\Tag(name="Reviews")
 * @Route("/v1/reviews")
 */
class ReviewsController
{
    private SerializerInterface $serializer;
    private RequestValidatorInterface $validator;
    private OvertimeDTOBuilder $overtimeDTOBuilder;
    private OvertimeDataReader $overtimeDataReader;

    public function __construct(
        SerializerInterface $serializer,
        RequestValidatorInterface $validator,
        OvertimeDTOBuilder $overtimeDTOBuilder,
        OvertimeDataReader $overtimeDataReader
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->overtimeDTOBuilder = $overtimeDTOBuilder;
        $this->overtimeDataReader = $overtimeDataReader;
    }

    /**
     * @Route("/overtime/{hotelId}", methods={"GET"})
     * @OA\Get(
     *     description="Overtime endpoint",
     *     summary="returns the overtime average score of the hotel for grouped date ranges",
     *     @OA\Parameter(
     *          name="hotelId",
     *          in="path",
     *          schema=@OA\Schema(type="integer"),
     *          description="Hotel ID",
     *          example="1",
     *          required=true,
     *     ),
     *     @OA\Parameter(
     *          name="from",
     *          in="query",
     *          schema=@OA\Schema(type="string"),
     *          description="From date",
     *          example="2021-01-12",
     *          required=true,
     *     ),
     *     @OA\Parameter(
     *          name="to",
     *          in="query",
     *          schema=@OA\Schema(type="string"),
     *          description="To date",
     *          example="2021-04-10",
     *          required=true,
     *     ),
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns a list of reviews",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=OvertimeReviewDTO::class)),
     *     )
     * )
     */
    public function overtime(int $hotelId, Request $request): JsonResponse
    {
        $dto = $this->overtimeDTOBuilder->build($hotelId, $request);

        $this->validator->validate($dto);

        $data = $this->overtimeDataReader->overtime($dto);

        return new JsonResponse(
            $this->serializer->serialize($data, 'json'),
            Response::HTTP_OK,
            [],
            true,
        );
    }
}
