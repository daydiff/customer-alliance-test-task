<?php

declare(strict_types=1);

namespace App\DTO\Request;

class OvertimeDTO
{
    private ?int $hotelId;
    private ?\DateTimeInterface $from;
    private ?\DateTimeInterface $to;

    /**
     * @return int|null
     */
    public function getHotelId(): ?int
    {
        return $this->hotelId;
    }

    /**
     * @param int|null $hotelId
     * @return OvertimeDTO
     */
    public function setHotelId(?int $hotelId): OvertimeDTO
    {
        $this->hotelId = $hotelId;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getFrom(): ?\DateTimeInterface
    {
        return $this->from;
    }

    /**
     * @param \DateTimeInterface|null $from
     * @return OvertimeDTO
     */
    public function setFrom(?\DateTimeInterface $from): OvertimeDTO
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTo(): ?\DateTimeInterface
    {
        return $this->to;
    }

    /**
     * @param \DateTimeInterface|null $to
     * @return OvertimeDTO
     */
    public function setTo(?\DateTimeInterface $to): OvertimeDTO
    {
        $this->to = $to;
        return $this;
    }
}
