<?php

declare(strict_types=1);

namespace App\DTO;

interface DateGroup
{
    public const DAY = 'day';
    public const WEEK = 'week';
    public const MONTH = 'month';
}
