<?php

declare(strict_types=1);

namespace App\DTO\Response;

class OvertimeReviewDTO
{
    /**
     * @var int|null Review count
     */
    private ?int $reviewCount;

    /**
     * @var int|null Average score
     */
    private ?int $averageScore;

    /**
     * @var string|null Date group (DAY|WEEK|MONTH)
     */
    private ?string $dateGroup;

    /**
     * @return int|null
     */
    public function getReviewCount(): ?int
    {
        return $this->reviewCount;
    }

    /**
     * @param int|null $reviewCount
     * @return OvertimeReviewDTO
     */
    public function setReviewCount(?int $reviewCount): OvertimeReviewDTO
    {
        $this->reviewCount = $reviewCount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAverageScore(): ?int
    {
        return $this->averageScore;
    }

    /**
     * @param int|null $averageScore
     * @return OvertimeReviewDTO
     */
    public function setAverageScore(?int $averageScore): OvertimeReviewDTO
    {
        $this->averageScore = $averageScore;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDateGroup(): ?string
    {
        return $this->dateGroup;
    }

    /**
     * @param string|null $dateGroup
     * @return OvertimeReviewDTO
     */
    public function setDateGroup(?string $dateGroup): OvertimeReviewDTO
    {
        $this->dateGroup = $dateGroup;
        return $this;
    }
}
