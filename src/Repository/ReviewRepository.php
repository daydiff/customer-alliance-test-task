<?php

declare(strict_types=1);

namespace App\Repository;

use App\DTO\DateGroup;
use App\Entity\Review;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[] findAll(array $criteria, array $orderBy = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    public function overtime($hotelId, $from, $to, $group)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb = $qb
            ->select(
                'COUNT(r.id) as reviewCount',
                'AVG(r.score) as averageScore',
            )
            ->from(Review::class, 'r')
            ->where(
                'r.hotel = :hotelId and r.createdDate >= :from and r.createdDate <= :to',
            )
            ->setParameter('hotelId', $hotelId)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->groupBy('dateGroup')
            ->orderBy('dateGroup')
        ;

        switch ($group) {
            case DateGroup::DAY:
                $qb->addSelect('DAY(r.createdDate) as dateGroup');
                break;
            case DateGroup::WEEK:
                $qb->addSelect('WEEK(r.createdDate) as dateGroup');
                break;
            case DateGroup::MONTH:
                $qb->addSelect('MONTH(r.createdDate) as dateGroup');
                break;
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws ORMException
     */
    public function save(Review $entity): void
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
    }
}
