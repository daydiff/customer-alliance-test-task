<?php

declare(strict_types=1);

namespace App\Builder;

use App\DTO\Request\OvertimeDTO;
use Symfony\Component\HttpFoundation\Request;

class OvertimeDTOBuilder
{
    public function build(int $hotelId, Request $request): OvertimeDTO
    {
        return (new OvertimeDTO())
            ->setHotelId($hotelId)
            ->setFrom(
                $request->query->has('from')
                    ? $this->parseDate($request->query->get('from'))
                    : null
            )
            ->setTo(
                $request->query->has('to')
                    ? $this->parseDate($request->query->get('to'))
                    : null
            )
            ;
    }

    private function parseDate($date): ?\DateTimeInterface
    {
        return \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $date . ' 00:00:00') ?: null;
    }
}
