<?php

declare(strict_types=1);

namespace App\DataReader;

use App\DTO\DateGroup;
use App\DTO\Request\OvertimeDTO;

class GroupDetector
{
    public function getGroup(OvertimeDTO $dto): string
    {
        $days = $dto->getFrom()->diff($dto->getTo())->format('%a');

        switch (true) {
            case $days <= 29:
                return DateGroup::DAY;
            case $days <= 89:
                return DateGroup::WEEK;
            default:
                return DateGroup::MONTH;
        }
    }
}
