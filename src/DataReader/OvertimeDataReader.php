<?php

declare(strict_types=1);

namespace App\DataReader;

use App\DTO\Request\OvertimeDTO;
use App\DTO\Response\OvertimeReviewDTO;
use App\Repository\ReviewRepository;

class OvertimeDataReader
{
    private ReviewRepository $repository;
    private GroupDetector $groupDetector;

    public function __construct(ReviewRepository $repository, GroupDetector $groupDetector)
    {
        $this->repository = $repository;
        $this->groupDetector = $groupDetector;
    }

    /**
     * @param OvertimeDTO $dto
     *
     * @return OvertimeReviewDTO[]
     */
    public function overtime(OvertimeDTO $dto): array
    {
        $group = $this->groupDetector->getGroup($dto);

        $results = $this->repository->overtime(
            $dto->getHotelId(),
            $dto->getFrom(),
            $dto->getTo(),
            $group,
        );

        return array_map(static function($item) use ($group) {
            return (new OvertimeReviewDTO())
                ->setAverageScore((int)$item['averageScore'])
                ->setReviewCount($item['reviewCount'])
                ->setDateGroup($group)
            ;
        }, $results);
    }
}
