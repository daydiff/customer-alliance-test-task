<?php

declare(strict_types=1);

namespace App\Tests\Builder;

use App\Builder\OvertimeDTOBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class OvertimeDTOBuilderTest extends TestCase
{
    private OvertimeDTOBuilder $cut;

    public function setUp(): void
    {
        $this->cut = new OvertimeDTOBuilder();
    }

    /**
     * @dataProvider data
     */
    public function testBuild(?int $hotelId, Request $request, ?\DateTimeInterface $expectedFrom, ?\DateTimeInterface $expectedTo)
    {
        $actual = $this->cut->build($hotelId, $request);

        $this->assertEquals($hotelId, $actual->getHotelId());
        $this->assertEquals($expectedFrom, $actual->getFrom());
        $this->assertEquals($expectedTo, $actual->getTo());
    }

    public function data()
    {
        # 1
        $from = '2020-03-12';
        $to = '2020-05-12';
        $request = new Request(
            [
                'from' => $from,
                'to' => $to,
            ],
        );
        yield [
            1,
            $request,
            \DateTime::createFromFormat('Y-m-d H:i:s', $from . ' 00:00:00'),
            \DateTime::createFromFormat('Y-m-d H:i:s', $to . ' 00:00:00'),
        ];

        # 2
        $from = '2020-23-43';
        $to = '2020-05-12';
        $request = new Request(
            [
                'from' => $from,
                'to' => $to,
            ],
        );
        yield [
            1,
            $request,
            \DateTime::createFromFormat('Y-m-d H:i:s', $from . ' 00:00:00'),
            \DateTime::createFromFormat('Y-m-d H:i:s', $to . ' 00:00:00'),
        ];

        # 3
        $from = 'foo';
        $to = 'bar';
        $request = new Request(
            [
                'from' => $from,
                'to' => $to,
            ],
        );
        yield [
            1,
            $request,
            null,
            null,
        ];

    }
}
