<?php

declare(strict_types=1);

namespace App\Tests\DataReader;

use App\DataReader\GroupDetector;
use App\DataReader\OvertimeDataReader;
use App\DTO\Request\OvertimeDTO;
use App\DTO\Response\OvertimeReviewDTO;
use App\Repository\ReviewRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;

class OvertimeDataReaderTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @var ObjectProphecy|ReviewRepository
     */
    private $repository;

    /**
     * @var OvertimeDataReader|ObjectProphecy
     */
    private $cut;

    /**
     * @var GroupDetector|ObjectProphecy
     */
    private $groupDetector;

    public function setUp(): void
    {
        $this->repository = $this->prophesize(ReviewRepository::class);
        $this->groupDetector = $this->prophesize(GroupDetector::class);

        $this->cut = new OvertimeDataReader(
            $this->repository->reveal(),
            $this->groupDetector->reveal(),
        );
    }

    /**
     * @dataProvider data
     */
    public function testSearch($hotelId, OvertimeDTO $requestDto, $items, $expectedGroup, $expected)
    {
        $this->repository
            ->overtime($hotelId, $requestDto->getFrom(), $requestDto->getTo(), $expectedGroup)
            ->shouldBeCalled()
            ->willReturn($items)
        ;

        $this->groupDetector
            ->getGroup($requestDto)
            ->shouldBeCalled()
            ->willReturn($expectedGroup)
        ;

        $actual = $this->cut->overtime($requestDto);

        $this->assertEquals($expected, $actual);
    }

    public function data()
    {
        # 1
        $hotelId = 1;
        $from = '2020-03-01';
        $to = '2020-03-31';
        $requestDto = (new OvertimeDTO())
            ->setHotelId($hotelId)
            ->setFrom(\DateTime::createFromFormat('Y-m-d H:i:s', $from . ' 00:00:00'))
            ->setTo(\DateTime::createFromFormat('Y-m-d H:i:s', $to . ' 00:00:00'))
        ;
        $averageScore = 3;
        $reviewCount = 23;
        $dateGroup = 'day';
        yield [
            $hotelId,
            $requestDto,
            [
                ['averageScore' => $averageScore, 'reviewCount' => $reviewCount]
            ],
            $dateGroup,
            [
                (new OvertimeReviewDTO())
                    ->setReviewCount($reviewCount)
                    ->setAverageScore($averageScore)
                    ->setDateGroup($dateGroup),
            ],
        ];

        # 2
        $hotelId = 2;
        $from = '2020-03-01';
        $to = '2020-03-31';
        $requestDto = (new OvertimeDTO())
            ->setHotelId($hotelId)
            ->setFrom(\DateTime::createFromFormat('Y-m-d H:i:s', $from . ' 00:00:00'))
            ->setTo(\DateTime::createFromFormat('Y-m-d H:i:s', $to . ' 00:00:00'))
        ;
        $dateGroup = 'day';
        yield [
            $hotelId,
            $requestDto,
            [],
            $dateGroup,
            [],
        ];
    }
}
