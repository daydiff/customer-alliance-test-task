<?php

declare(strict_types=1);

namespace App\Tests\DataReader;

use App\DataReader\GroupDetector;
use App\DTO\Request\OvertimeDTO;
use PHPUnit\Framework\TestCase;

class GroupDetectorTest extends TestCase
{

    /**
     * @dataProvider data
     */
    public function testGetGroup(OvertimeDTO $dto, $expected)
    {
        $cut = new GroupDetector();
        $actual = $cut->getGroup($dto);

        $this->assertEquals($expected, $actual);
    }

    public function data()
    {
        return [
            [
                (new OvertimeDTO)
                    ->setFrom(\DateTimeImmutable::createFromFormat( 'Y-m-d','2021-01-01'))
                    ->setTo(\DateTimeImmutable::createFromFormat( 'Y-m-d','2021-01-29')),
                'day',
            ],
            [
                (new OvertimeDTO)
                    ->setFrom(\DateTimeImmutable::createFromFormat( 'Y-m-d','2021-01-01'))
                    ->setTo(\DateTimeImmutable::createFromFormat( 'Y-m-d','2021-02-31')),
                'week',
            ],
            [
                (new OvertimeDTO)
                    ->setFrom(\DateTimeImmutable::createFromFormat( 'Y-m-d','2021-01-01'))
                    ->setTo(\DateTimeImmutable::createFromFormat( 'Y-m-d','2021-04-31')),
                'month',
            ],
        ];
    }
}
