<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210804102351 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE `hotels` (
                `id`                     bigint       UNSIGNED NOT NULL AUTO_INCREMENT,
                `name`                   varchar(255) NOT NULL,
                `created_date`           datetime     DEFAULT  CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            )
        ');
        $this->addSql('
            CREATE TABLE `reviews` (
                `id`                     bigint       UNSIGNED NOT NULL AUTO_INCREMENT,
                `hotel_id`               bigint       UNSIGNED NOT NULL,
                `score`                  int(1)       NOT NULL,
                `comment`                text         NOT NULL,
                `created_date`           datetime     DEFAULT  CURRENT_TIMESTAMP,
                PRIMARY KEY (`id`)
            )
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE `hotels`');
        $this->addSql('DROP TABLE `reviews`');
    }
}
