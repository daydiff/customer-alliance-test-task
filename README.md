# CA Test Task

## How to start

- Run from the project root:

```
docker-compose build
docker-compose up -d
docker-compose run php /usr/local/bin/composer -d/app install
docker-compose run php /app/bin/console doctrine:migrations:migrate -n
docker-compose run php /app/bin/console generate:data
```
- Open [http://localhost:8001/api/doc](http://localhost:8001/api/doc);


## Notes
The current data model leads to temporary tables being used by MySQL:
```
-> Table scan on <temporary>  (actual time=0.001..0.003 rows=31 loops=1)
   -> Aggregate using temporary table  (actual time=25.665..25.669 rows=31 loops=1)
      -> Filter: (r0_.hotel_id = 1)  (cost=10698.56 rows=2377) (actual time=0.264..24.646 rows=1274 loops=1)
         -> Index range scan on r0_ using created_date_idx, with index condition: ((r0_.created_date >= DATE'2021-01-12') and (r0_.created_date <= DATE'2021-04-10'))  (cost=10698.56 rows=23774) (actual time=0.261..23.744 rows=12283 loops=1)

```

For this amount of data it works quite fast but if it increased by order of magnitude then we might need to use projections with pre-calculated values.
